package eemi.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private TextView result;
    private CalcBrain brain = new CalcBrain(); // ?
    private boolean clearOnType = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        result = (TextView) findViewById(R.id.result);

        View.OnClickListener numberClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button)v;
                if(!clearOnType) // ?
                    result.setText(result.getText().toString()+b.getText().toString());
                else {
                    result.setText(b.getText().toString());
                    clearOnType = false;
                }
            }
        };

        findViewById(R.id.b0).setOnClickListener(numberClickListener);
        findViewById(R.id.b1).setOnClickListener(numberClickListener);
        findViewById(R.id.b2).setOnClickListener(numberClickListener);
        findViewById(R.id.b3).setOnClickListener(numberClickListener);
        findViewById(R.id.b4).setOnClickListener(numberClickListener);
        findViewById(R.id.b5).setOnClickListener(numberClickListener);
        findViewById(R.id.b6).setOnClickListener(numberClickListener);
        findViewById(R.id.b7).setOnClickListener(numberClickListener);
        findViewById(R.id.b8).setOnClickListener(numberClickListener);
        findViewById(R.id.b9).setOnClickListener(numberClickListener);

        // special case, only one . allowed
        findViewById(R.id.bDot).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(result.getText().toString().contains("."))
                    return; // ?

                result.setText(result.getText().toString()+".");
            }
        });

        View.OnClickListener opListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button)v;
                if(clearOnType) {
                    brain.replaceOperation(b.getText().toString()); // ?
                    return;
                }
                brain.pushValue(Double.parseDouble(result.getText().toString()));
                brain.calc();
                result.setText(String.valueOf(brain.getValue()));

                brain.pushOperation(b.getText().toString()); // ?
                clearOnType = true;
            }
        };

        View.OnClickListener unopListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button)v;
                if(!clearOnType)
                    brain.pushValue(Double.parseDouble(result.getText().toString())); // ?
                brain.pushOperation(b.getText().toString());
                brain.calc();
                result.setText(String.valueOf(brain.getValue()));
                clearOnType = true;
            }
        };

        // binary operations
        findViewById(R.id.bPlus).setOnClickListener(opListener);
        findViewById(R.id.bMoins).setOnClickListener(opListener);
        findViewById(R.id.bMult).setOnClickListener(opListener);
        findViewById(R.id.bDiv).setOnClickListener(opListener);

        // unary operations
        findViewById(R.id.bcos).setOnClickListener(unopListener);
        findViewById(R.id.bSin).setOnClickListener(unopListener);
        findViewById(R.id.bSqrt).setOnClickListener(unopListener);

        // clear
        findViewById(R.id.bClear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                brain.clear();
                result.setText("");
            }
        });

        // equals
        findViewById(R.id.bEq).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                brain.pushValue(Double.parseDouble(result.getText().toString()));
                brain.calc();
                result.setText(String.valueOf(brain.getValue()));
                clearOnType = true;
            }
        });
    }
}
