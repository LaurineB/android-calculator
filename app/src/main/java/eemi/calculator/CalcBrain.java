package eemi.calculator;

import java.util.Stack;

public class CalcBrain {
    interface Operation {
        void evaluate();
    }
    private Stack<Double> values = new Stack<>(); // ?
    private Stack<Operation> ops = new Stack<>(); // ?

    public void clear() { values.clear(); ops.clear(); }
    public double getValue() {
        if(values.size() > 0)
            return values.peek();
        else
            return 0;
    }

    public void pushValue(double d) {
        values.push(d);
    }

    public void pushOperation(String s) {
        if(s.equalsIgnoreCase("+")) {
            ops.push(new Operation() {
                @Override
                public void evaluate() {
                    if(values.size() > 1)
                        values.push(values.pop() + values.pop());
                }
            });
        } else if(s.equalsIgnoreCase("-")) {
            ops.push(new Operation() {
                @Override
                public void evaluate() {
                    if(values.size() > 1) {
                        double lastval = values.pop();
                        double prevval = values.pop();
                        values.push( prevval - lastval );
                    }
                }
            });
        } else if(s.equalsIgnoreCase("*")) {
            ops.push(new Operation() {
                @Override
                public void evaluate() {
                    if(values.size() > 1)
                        values.push(values.pop() * values.pop());
                }
            });
        } else if(s.equalsIgnoreCase("/")) {
            ops.push(new Operation() {
                @Override
                public void evaluate() {
                    if(values.size() > 1) {
                        double lastval = values.pop();
                        double prevval = values.pop();
                        values.push( prevval / lastval );
                    }
                }
            });
        } else if(s.equalsIgnoreCase("√")) {
            ops.push(new Operation() {
                @Override
                public void evaluate() {
                    if(values.size() > 0)
                        values.push(Math.sqrt(values.pop()));
                }
            });
        } else if(s.equalsIgnoreCase("cos")) {
            ops.push(new Operation() {
                @Override
                public void evaluate() {
                    if(values.size() > 0)
                        values.push(Math.cos(values.pop()));
                }
            });
        } else if(s.equalsIgnoreCase("sin")) {
            ops.push(new Operation() {
                @Override
                public void evaluate() {
                    if(values.size() > 0)
                        values.push(Math.sin(values.pop()));
                }
            });
        }
    }

    public void replaceOperation(String s) {
        if(ops.size() > 0)
            ops.pop();
        pushOperation(s);
    }

    public void calc() {
        if(ops.size() > 0) {
            Operation op = ops.pop();
            op.evaluate();
        }
    }
}
